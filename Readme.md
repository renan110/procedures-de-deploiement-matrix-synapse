**SAE déploiement d'une application**

**Groupe J : Declercq Renan - Delerue Clement**

## Explication du projet

### Scénario 

Une entreprise de développement logiciel souhaite améliorer les modes de communication à la fois des collaborateurs entre eux, mais également entre les collaborateurs et les clients de l’entreprise (notamment à des fins de support).

Pour cela, elle souhaite mettre en place un outil répondant aux contraintes suivantes:

- Le mode de communication proposé doit supporter une conversation synchrone et asynchrone ;
- Une conversation peut avoir lieu entre deux personnes (communication 1 vers 1), en petit groupe (discussion entre développeurs) ou en audience plus large (forum de discussion) 
- Les communications doivent être chiffrées de bout en bout afin d’assurer la confidentialité des échanges 
le service de discussion doit pouvoir être accessible en utilisant une application web, une application lourde ou une application mobile.
Après une étude des solutions existantes, le groupe de travail chargé de choisir la solution à déployer vous demande de mettre en place une solution basée sur le standard Matrix qui répond à tous les critères demandés.

### Matrix

Matrix est un projet open source qui publie un standard pour une communication sécurisée, distribuée et temps réelle.

Un standard est l’ensemble des documentations de référence permettant à plusieurs implémentations respectant celui-ci de se comprendre. En d’autres termes, il peut y avoir plusieurs logiciels qui implémentent le même standard. Ces logiciels vont pouvoir inter-opérer car il se comprennent, parle la même langue, grâce au standard.

Le standard matrix défini la liste et le format de message permettant:

à un serveur de discussion (back-end) de dialoguer avec un client (front-end). Ainsi, plusieurs client différents (web, mobile, application lourde, client simple en mode texte…) peuvent communiquer avec le serveur, laissant le choix de l’interface à l’utilisateur ;
à plusieurs serveurs de discussion de fédérer leurs utilisateurs et leur canaux. Ainsi, les utilisateurs de différents fournisseurs pourront communiquer entre eux.

### Implémentation de réference 

En plus du standard, la fondation matrix propose une implémentation de référence du standard: Synapse, un serveur écrit en python. C’est cette version du serveur que vous allez devoir mettre en place.

Pour la partie cliente, c’est la solution Element Web que vous devrez proposer.

Pour réaliser cette SAÉ, vous allez travailler sur des machines virtuelles. Contrairement à ce que vous avez utilisé au département jusqu’à présent, il n’est pas question d’utiliser des machines avec interface graphique, mais de travailler à distance sur les serveurs que vous allez administrer.

## Si vous consultez les procédures en Markdown

Nous utilisons les admonitions markdown, pour les visualiser il faut télécharger une extension tel que **Markdown all in one** ou encore **Markdown Preview Enhanced**.

Nous comptions convertir les documents markdown en HTML , ainsi il n'y aurait pas eu de soucis de comptabilité mais nous avons vu la dernière consigne qui consiste à ne pas fournir de document HTML dans le dossier à rendre. 


On vous conseille de consulter les procédures en via les documents HTML !

